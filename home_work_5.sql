-- использовать БД "shop"
use shop;

-- эти данные заполнены с использованием графического интерфейса
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('1', 'Zara');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('2', 'OGGI');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('3', 'Sela');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('4', 'Colin\'s');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('5', 'Mango');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('6', 'Topshop');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('7', 'New Look');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('8', 'Savage');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('9', 'Adidas');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('10', 'Nike');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('11', 'Puma');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('12', 'Reebok');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('13', 'Columbia');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('14', 'Esprit');
INSERT INTO `shop`.`brand` (`id`, `name`) VALUES ('15', 'Converse');

-- эти данные заполнены с использованием SQL-комманды и учетом автоинкремента в поле ID
INSERT INTO product_type (name) VALUES ('Костюм классический');
INSERT INTO product_type (name) VALUES ('Платье вечернее');
INSERT INTO product_type (name) VALUES ('Рубашка');
INSERT INTO product_type (name) VALUES ('Брюки');
INSERT INTO product_type (name) VALUES ('Галстук');
INSERT INTO product_type (name) VALUES ('Мокасины');
INSERT INTO product_type (name) VALUES ('Шуба женская');
INSERT INTO product_type (name) VALUES ('Куртка демисезонная');
INSERT INTO product_type (name) VALUES ('Джинсы');
INSERT INTO product_type (name) VALUES ('Футболка');
INSERT INTO product_type (name) VALUES ('Туфли');
INSERT INTO product_type (name) VALUES ('Ботинки зимние');
INSERT INTO product_type (name) VALUES ('Ботинки демисезонные');
INSERT INTO product_type (name) VALUES ('Шуба');
INSERT INTO product_type (name) VALUES ('Шорты');
INSERT INTO product_type (name) VALUES ('Майка');
INSERT INTO product_type (name) VALUES ('Трусы');
INSERT INTO product_type (name) VALUES ('Плавки');
INSERT INTO product_type (name) VALUES ('Костюм спортивный');
INSERT INTO product_type (name) VALUES ('Бейсболка');
INSERT INTO product_type (name) VALUES ('Плащ');
INSERT INTO product_type (name) VALUES ('Шляпка летняя');
INSERT INTO product_type (name) VALUES ('Шляпа классическая');
INSERT INTO product_type (name) VALUES ('Пиджак');
INSERT INTO product_type (name) VALUES ('Трико');
INSERT INTO product_type (name) VALUES ('Толстовка');
INSERT INTO product_type (name) VALUES ('Куртка-ветровка');